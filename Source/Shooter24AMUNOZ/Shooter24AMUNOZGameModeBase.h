// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Shooter24AMUNOZGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER24AMUNOZ_API AShooter24AMUNOZGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
