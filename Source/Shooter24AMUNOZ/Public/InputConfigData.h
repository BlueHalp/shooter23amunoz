﻿
#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputConfigData.generated.h"

/**
 * 
 */
class UInputAction;

UCLASS()
class SHOOTER24AMUNOZ_API UInputConfigData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputMove;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputLook;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputJump;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputMouseClick;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputSprint;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputCrouch;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* InputDropWeapon;
};
