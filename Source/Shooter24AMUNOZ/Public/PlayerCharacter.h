// Fill out your copyright notice in the Description page of Project Settings.

// TAREA1: Hacer sistema para agacharse (A medias)
// TAREA2: Poder dropear armas y recoger otra vez
#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "Weapon_BlueprintComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class UInputMappingContext;
class UInputConfigData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClick);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDropWeapon);

UCLASS()
class SHOOTER24AMUNOZ_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerCharacter();

	virtual void Tick(float DeltaTime) override;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UStaticMeshComponent* GetWeapon() const { return vWeaponPoint; }

	UCameraComponent* GetCamera() const { return vCamera; }
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* vWeaponPoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputMappingContext* vInputMapping;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputConfigData* vInputData;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int vSpeedModifier {4U};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int vDefaultWalkingSpeed {0U};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float vCurrentHP {100.f};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float vMaxHP { 100.f};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool vIsRunning { false };


	UPROPERTY(BlueprintAssignable)
		FOnClick evOnClick;
	
	UPROPERTY(BlueprintAssignable)
		FOnDropWeapon evOnDropWeapon;

	UWeapon_BlueprintComponent* vWeaponEquipped;

protected:
	virtual void BeginPlay() override;

private:

	void MoveCallback(const FInputActionValue& mValue);

	void LookCallback(const FInputActionValue& mValue);

	void OnClickCallback (const FInputActionValue& mValue);

	void OnSprintCallback (const FInputActionValue& mValue);

	void onStopSprintCallback();

	void onCrouchCallback(const FInputActionValue& mValue);

	void onDropWeaponCallback();

	UPROPERTY(EditDefaultsOnly)
	UCameraComponent* vCamera;

};
