// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "Weapon_BlueprintComponent.generated.h"

class APlayerCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER24AMUNOZ_API UWeapon_BlueprintComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UWeapon_BlueprintComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void AttachWeapon(APlayerCharacter* mPlayerCharacter);

	UFUNCTION(BlueprintCallable)
	void DropWeapon();
	
	UFUNCTION(BlueprintCallable)
	void OnFireCallback();

	UPROPERTY(EditDefaultsOnly)
	UBillboardComponent* vMuzzleOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float vShootRange {5000.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	double vBulletSpread {0.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	double vBulletsPerShot {1};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float vDamage {2.f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	APlayerCharacter* vOwnerCharacter;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
};
