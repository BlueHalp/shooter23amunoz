// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class SHOOTER24AMUNOZ_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemyCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly)
		class UBoxComponent* vBoxCollision;

	UFUNCTION()
		void OnHitCallback(AActor* mDamagedActor, float mDamage, const UDamageType* mDamageType, AController* mInstigatedBy, AActor* mDamageCauser);

	UPROPERTY(EditDefaultsOnly)
		float vMoveSped {400.f};

	UPROPERTY(EditDefaultsOnly)
		float vDamage {5.f};

#pragma region IA

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UAIPerceptionComponent* vIAPerception;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UAISenseConfig_Sight* vIASight;

	UFUNCTION()
	void OnSensedCallback(const TArray<AActor*>& Actors);

#pragma endregion
	
protected:
	virtual void BeginPlay() override;

private:
	void SetNewRotation (const FVector& aTargetRotation);

	UPROPERTY(VisibleAnywhere)
		FRotator vCurrentRotator;

	UPROPERTY(VisibleAnywhere)
		FVector vSpawnLocation;

	UPROPERTY(VisibleAnywhere)
		FVector vCurrentVelocity;

	bool vGoToBase;
	FVector vNewPosition;
	float vDistanceToPlayer;
	

};
