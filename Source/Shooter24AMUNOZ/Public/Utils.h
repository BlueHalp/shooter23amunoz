﻿#pragma once

//Propias macros para facilitar la programación (son como metodos)
#define ScreenPrint(x) if (GEngine != nullptr) { GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, x); }