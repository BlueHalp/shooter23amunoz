
#include "Shooter24AMUNOZ/Public/PlayerCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "Utils.h"
#include "Components/CapsuleComponent.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "Shooter24AMUNOZ/Public/Utils.h"

APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	vWeaponPoint = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponPoint"));

	vWeaponPoint->bCastDynamicShadow = false;
	vWeaponPoint->CastShadow = false;

	vCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
	vCamera->SetupAttachment(GetCapsuleComponent());
	vCamera->bUsePawnControlRotation = true;
	GetCharacterMovement()->CrouchedHalfHeight = 40.f;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	vWeaponPoint->AttachToComponent(GetMesh(), {EAttachmentRule::SnapToTarget, true}, TEXT("WeaponPointSocket"));
	
	vDefaultWalkingSpeed = GetCharacterMovement()->MaxWalkSpeed;
}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	
	APlayerController* PlayerController = Cast<APlayerController>(GetController());
	UEnhancedInputLocalPlayerSubsystem* EInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
	EInputSubsystem->ClearAllMappings();
	EInputSubsystem->AddMappingContext(vInputMapping, 0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(PlayerInputComponent)};
	EInputComponent->BindAction(vInputData->InputMove, ETriggerEvent::Triggered, this, &APlayerCharacter::MoveCallback);
	EInputComponent->BindAction(vInputData->InputLook, ETriggerEvent::Triggered, this, &APlayerCharacter::LookCallback);
	EInputComponent->BindAction(vInputData->InputJump, ETriggerEvent::Triggered, this, &ACharacter::Jump);
	EInputComponent->BindAction(vInputData->InputMouseClick, ETriggerEvent::Started, this, &APlayerCharacter::OnClickCallback);
	EInputComponent->BindAction(vInputData->InputSprint, ETriggerEvent::Triggered, this, &APlayerCharacter::OnSprintCallback);
	EInputComponent->BindAction(vInputData->InputSprint, ETriggerEvent::Completed, this, &APlayerCharacter::onStopSprintCallback);
	EInputComponent->BindAction(vInputData->InputCrouch, ETriggerEvent::Triggered, this, &APlayerCharacter::onCrouchCallback);
	EInputComponent->BindAction(vInputData->InputDropWeapon, ETriggerEvent::Triggered, this, &APlayerCharacter::onDropWeaponCallback);
	
}


void APlayerCharacter::MoveCallback(const FInputActionValue& mValue)
{
	if (IsValid(Controller))
	{
		const FVector2D MoveValue {mValue.Get<FVector2D>()};
		const FRotator MoveRotator {0,Controller->GetControlRotation().Yaw,0};

		if (MoveValue.Y != 0.0f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::ForwardVector)};
			AddMovementInput(Dir, MoveValue.Y);
		}
		if (MoveValue.X != 0.0f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::RightVector)};
			AddMovementInput(Dir, MoveValue.X);
		}
	}
}

void APlayerCharacter::OnSprintCallback(const FInputActionValue& mValue)
{
	if (IsValid(Controller))
	{
		vWeaponPoint->SetVisibility(false);
		vIsRunning = true;
		GetCharacterMovement()->MaxWalkSpeed = vSpeedModifier * vDefaultWalkingSpeed + vSpeedModifier;
	}
}

void APlayerCharacter::onStopSprintCallback()
{
	if (IsValid(Controller))
	{
		vWeaponPoint->SetVisibility(true);
		vIsRunning = false;
		GetCharacterMovement()->MaxWalkSpeed = vSpeedModifier + vDefaultWalkingSpeed;
	}
}

void APlayerCharacter::onCrouchCallback(const FInputActionValue& mValue)
{
	if (IsValid(Controller))
	{
		this->Crouch();
	}
}

void APlayerCharacter::LookCallback(const FInputActionValue& mValue)
{
	if (IsValid(Controller))
	{
		const FVector2D LookValue {mValue.Get<FVector2d>()};
		if (LookValue.X != 0)
		{
			AddControllerYawInput(LookValue.X);
		}
		if (LookValue.Y != 0)
		{
			AddControllerPitchInput(LookValue.Y);
		}
	}
}


void APlayerCharacter::OnClickCallback(const FInputActionValue& mValue)
{
	if (IsValid(Controller))
	{
		evOnClick.Broadcast();
		if (GEngine != nullptr)
		{
			// ScreenPrint(FString::Printf(TEXT("Hola %d"), vMoveSpeed));
		}
	}
}

void APlayerCharacter::onDropWeaponCallback()
{
	if (IsValid(Controller))
	{
		evOnDropWeapon.Broadcast();
	}
}
