
#include "EnemyCharacter.h"

#include "PlayerCharacter.h"
#include "Components/BoxComponent.h"
#include "Shooter24AMUNOZ/Public/Utils.h"

AEnemyCharacter::AEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	vBoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	vBoxCollision->SetupAttachment(RootComponent);

	vIASight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AISighConfig"));
	vIASight->SightRadius =  1240.f;
	vIASight->LoseSightRadius = 1300.f;
	vIASight->PeripheralVisionAngleDegrees = 90.f;
	vIASight->DetectionByAffiliation.bDetectEnemies = true;
		vIASight->DetectionByAffiliation.bDetectFriendlies = true;
		vIASight->DetectionByAffiliation.bDetectNeutrals = true;
	vIASight->SetMaxAge(.1f);

	vIAPerception = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	vIAPerception->ConfigureSense(*vIASight);
	vIAPerception->SetDominantSense(vIASight->GetSenseImplementation());
	vIAPerception->OnPerceptionUpdated.AddDynamic(this, &AEnemyCharacter::OnSensedCallback);

	vCurrentVelocity = FVector::ZeroVector;

	vDistanceToPlayer = BIG_NUMBER;
	
}

void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!vCurrentVelocity.IsZero())
	{
		vNewPosition = GetActorLocation() + vCurrentVelocity * DeltaTime;

		if (vGoToBase)
		{
			double SQRT = (vNewPosition - vSpawnLocation).SizeSquared2D();
			if (SQRT < vDistanceToPlayer)
			{
				vDistanceToPlayer = SQRT;
			}
			else
			{
				vDistanceToPlayer = BIG_NUMBER;
				vGoToBase = false;
				SetNewRotation(GetActorForwardVector());
				vCurrentVelocity = FVector::ZeroVector;
			}
		}
		SetActorLocation(vNewPosition);
	}

}

void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacter::OnHitCallback(AActor* mDamagedActor, float mDamage, const UDamageType* mDamageType, AController* mInstigatedBy, AActor* mDamageCauser)
{
	mDamagedActor->Destroy();
}

void AEnemyCharacter::OnSensedCallback(const TArray<AActor*>& Actors)
{
	
	for (const auto& UpdatedActor : Actors)
	{
		FActorPerceptionBlueprintInfo PerceptionInfo;

		vIAPerception->GetActorsPerception(UpdatedActor, PerceptionInfo);
		
		//Si encuentra al jugador
		if (PerceptionInfo.LastSensedStimuli[0].WasSuccessfullySensed() && Cast<APlayerCharacter>(UpdatedActor))
		{
			SetNewRotation(UpdatedActor->GetActorLocation());
		}

		//Volver a su punto incial
		else
		{
			
			FVector auxDirection {vSpawnLocation - GetActorLocation()};
			auxDirection.Z = 0;
			if (auxDirection.SizeSquared2D() > 1.0f)
			{
				vGoToBase = true;
				SetNewRotation(vSpawnLocation);
			}
		}
		
	}
}

void AEnemyCharacter::SetNewRotation(const FVector& aTargetRotation)
{
	FVector NewDir = aTargetRotation - GetActorLocation();
	NewDir.Z = 0;
	vCurrentVelocity = NewDir.GetSafeNormal() * vMoveSped;
	vCurrentRotator = NewDir.Rotation();
	SetActorRotation(vCurrentRotator);
}



