// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_BlueprintComponent.h"
#include "Utils.h"
#include "PlayerCharacter.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/Vector.h"
#include "Shooter24AMUNOZ/Public/Utils.h"


UWeapon_BlueprintComponent::UWeapon_BlueprintComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	vMuzzleOffset = CreateDefaultSubobject<UBillboardComponent>(TEXT("ShootPoint"));
}


// Called when the game starts
void UWeapon_BlueprintComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

void UWeapon_BlueprintComponent::AttachWeapon(APlayerCharacter* mPlayerCharacter)
{
	vOwnerCharacter = mPlayerCharacter;
	if (vOwnerCharacter != nullptr && vOwnerCharacter->vWeaponEquipped == nullptr)
	{
		vOwnerCharacter-> vWeaponEquipped = this;
		FAttachmentTransformRules AttachRules {EAttachmentRule::SnapToTarget, true};
		GetOwner()->AttachToComponent(vOwnerCharacter->GetWeapon(), AttachRules, FName(TEXT("WeaponPointSocket")));

		//Asociar evento disparar y dropear
		vOwnerCharacter->evOnClick.AddDynamic(this, &UWeapon_BlueprintComponent::OnFireCallback);
		vOwnerCharacter->evOnDropWeapon.AddDynamic(this, &UWeapon_BlueprintComponent::DropWeapon);
	}
}

void UWeapon_BlueprintComponent::DropWeapon()
{
	if (vOwnerCharacter != nullptr && vOwnerCharacter->vWeaponEquipped != nullptr)
	{
		vOwnerCharacter-> vWeaponEquipped = nullptr;
		FDetachmentTransformRules DetachRules {FDetachmentTransformRules::KeepWorldTransform};
		GetOwner()->DetachFromActor(DetachRules);
		vOwnerCharacter->evOnClick.RemoveDynamic(this, &UWeapon_BlueprintComponent::OnFireCallback);
		vOwnerCharacter->evOnClick.RemoveDynamic(this, &UWeapon_BlueprintComponent::DropWeapon);
		vOwnerCharacter = nullptr;
	}
}


void UWeapon_BlueprintComponent::OnFireCallback()
{
	if (vOwnerCharacter == nullptr) return;

	const UWorld* pWorld {GetWorld()};
	
	if (pWorld == nullptr) return;

	//Desde el centro de la camara
	//const FVector StartLocation { PlayerCamera->GetComponentLocation() };

	//Desde el cañon del arma
	
	const UCameraComponent* PlayerCamera {vOwnerCharacter->GetCamera()};

	const FRotator CameraRotation {PlayerCamera->GetComponentRotation()};
	const FVector StartLocation { GetOwner()->GetActorLocation() + CameraRotation.RotateVector(vMuzzleOffset->GetComponentLocation()) };
	FVector EndLocation { StartLocation + UKismetMathLibrary::GetForwardVector(PlayerCamera->GetComponentRotation()) * vShootRange};

	FCollisionQueryParams QueryParams {};
	QueryParams.AddIgnoredActor(vOwnerCharacter);

	FHitResult HitResult {};
	
	const FVector EndLocationOG = EndLocation;
	for (int i = 0; i < vBulletsPerShot; i++)
	{
		double Xspread = FMath::RandRange(EndLocation.X, EndLocation.X +vBulletSpread);
		double YSpread =FMath::RandRange(EndLocation.Y, EndLocation.Y +vBulletSpread);
		double ZSpread =FMath::RandRange(EndLocation.Z, EndLocation.Z +vBulletSpread);
		EndLocation.Set(Xspread, EndLocation.Y, ZSpread);
		
		pWorld->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Camera, QueryParams);
		DrawDebugLine(pWorld, StartLocation, EndLocation, HitResult.bBlockingHit ? FColor:: Blue : FColor::Red, false, 5.0f, 0, 10.f);
		EndLocation = EndLocationOG;
	}

	if (HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
	{
		UGameplayStatics::ApplyDamage(HitResult.GetActor(), vDamage, vOwnerCharacter->GetController(), GetOwner(), {});
	}
}

// Called every frame
void UWeapon_BlueprintComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

