# Shooter23AMunoz - Alejandro Muñoz

## Risketos Basics:

- [X] Control de personatge amb base ACharacter (o amb AController).
- [X] Capacitat de disparar, ja sigui amb Trace line (Raycast) o amb objectes físics en moviment (Projectile).
- [X] Utilitzar delegats per fer alguna acció, per exemple el disparar.
- [X] Més d’un tipus d'armes i/o bales.
- [X] Ús de les UMacros per privatització, escritura i lectura de forma correcta.

## Risketos Opcionals:

- [ ] En cas de bales físiques, fer una pool.
- [X] Control de jugador avançat.
- [X] Us d’Animation Blueprint i Blendtrees.
- [ ] Shaders.
- [X] Deixar armes i agafar-les
- [X] Ha d’implementar una petita inteligencia artificial als enemics.

## Controls

- Left Click (Amb arma) -> Disparar
- WASD -> Movimient
- F (Amb arma) -> Deixar arma
- Left Control -> Ajupir-se
- Left Shift -> Córrer